<?php
define('DB_NAME', '');
define('DB_USER', '');
define('DB_PASSWORD', '');
define('DB_HOST', '127.0.0.1');

function db_connect() {
    $connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($connection->connect_errno) {
        die ("Oopsie! Error connecting to the database: ". $connection->connect_errno. " => ". $connection->connect_error);
    }
    return $connection;
}

function db_sql($sql) {
    $connection = db_connect();
    if (!$result = $connection->query($sql)) { 
        die ('Dammit! Error: '. $connection->error. "<br />" . $sql);
    }
    return $result;
}

function db_select($tables, $fields = "*", $where = FALSE) {
    $where = ($where == FALSE) ? "" : " WHERE ".$where;
    $sql = "SELECT $fields FROM $tables $where";
    $result = db_sql($sql);
    if ($result->num_rows==0) $result=FALSE;    
    return $result;
}

function db_insert($table, $fields = array()) {
    $sql = "INSERT INTO $table ";
    $sqlfields = "";
    $sqlvalues = "";
    $first = "";
    foreach ($fields as $field => $value) {
        $sqlfields .= $first.$field;
        $value = db_sanitize($value);
        $sqlvalues .= $first."'$value'";
        if ($first == "") $first = ", ";
    }
    $sql .= "( $sqlfields ) VALUES ( $sqlvalues )";
    $result = db_sql($sql);
    return $result;
}

function db_update($table, $fields = array(), $where = "") {

    if ($where != "") $where = " WHERE ".$where;
    $sql = "UPDATE $table SET ";
    $update = "";
    $first = "";
    foreach ( $fields as $field => $value) {
        $value = db_sanitize($value);
        $update .= $first . "$field = '$value'";
        if ($first == "") $first = ", ";
    }
    $sql .= $update.$where;
    $result = db_sql($sql);
    return $result;    
}

function db_delete($table, $where = "") {
    if ($where != "") $where = " WHERE ".$where;
    $sql = "DELETE FROM $table $where";
    $result = db_sql($result);
    return $result;
}

/* The next two functions are by Chris Coyier http://css-tricks.com/snippets/php/sanitize-database-inputs/ */

function db_sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = db_sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysql_real_escape_string($input);
    }
    return $output;
}

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
}